<?php

namespace DigiOwl\Importer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class ImportCsv extends Command
{
    protected $csv;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:import 
                            {table? : The database table that you want to import to} 
                            {filePath? : Where the csv files is}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import CSV data to config seeder files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!$table = $this->argument('table'))
        {
            $table = $this->ask('Which table do you want to import to?');
        }

        if(!$filePath = $this->argument('filePath'))
        {
            $filePath = $this->ask('Where is your CSV located?');
        }


        if(!$this->checkDbTable($table))
        {
            $this->error('Table does not exist in DB');
            return 0;
        }


        if(!$this->checkCsvFile($filePath))
        {
            $this->error('File does not exist');
            return 0;
        }

        $this->info('FilePath: '.$filePath);

        $this->loadCsv($filePath)
             ->importCsv()
             ->csvAddHeaders($table)
             ->seedDatabase($table);
    }

    protected function checkDbTable($table)
    {
        return Schema::hasTable($table);
    }

    protected function checkCsvFile($filePath)
    {
        $this->info('./'.$filePath);
        return file_exists('./'.$filePath);
    }

    protected function loadCsv($filePath)
    {
        $row = 1;
        if (($handle = fopen("./".$filePath, "r")) !== FALSE) 
        {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
            {
                $num = count($data);

                $this->csv[] = $data;
                $row++;
            }
            fclose($handle);
        }
        return $this;
    }

    protected function importCsv()
    {
        if($headers = $this->confirm('Remove first line of CSV? (Headers)'))
        {
            unset($this->csv[0]);
        }

        $this->cleanCsv();

        return $this;
    }

    protected function cleanCsv()
    {
        $csv = [];

        $columns = $this->ask('Which Columns would you like to keep? Seperate your numbers by a comma i.e. 0,1,2,3,4');

        $delimiter = $this->ask('What delimiter would you like to use?');
        $columns = explode(',', $columns); 

        foreach($this->csv as $index => $rows)
        {
            $rows = explode($delimiter, $rows[0]);

            foreach($rows as $key => $row)
            {
                if(in_array($key, $columns)) 
                {
                    $csv[$index][] = trim($row, '"');
                }
            }
        }

        $this->csv = $csv;
    }

    /**
     * csvAddHeaders
     *
     * Map the csv to database tables
     *
     * @access protected
     * @return void
     */
    protected function csvAddHeaders($table)
    {
        $csv = [];
        $headers = [];
        $dbSchema = $this->getDbHeaders($table);
        $dbSchema[] = 'Skip';

        foreach($this->csv[array_key_first($this->csv)] as $key => $example)
        {
            if(count($dbSchema) === 1)
            {
                break;
            }

            $this->info('Which header is this: ');

            $choice = $this->choice('Key:'.$key.' Value: '.$example, 
                $dbSchema);
            // Remove choice from headers
            if($choice === 'Skip')
            {
                continue;
            }

            $headers[$key] = $choice;
            $dbSchema = array_diff($dbSchema, [$choice]);
        }

        foreach($this->csv as $index => $row)
        {
            foreach($headers as $key => $header)
            {
                $csv[$index][$header] = $row[$key];
            }
        }

        $this->csv = $csv;

        return $this;
    }

    protected function getDbHeaders($table)
    {
        return Schema::getColumnListing($table);
    }

    protected function seedDatabase($table)
    {
        foreach($this->csv as $record)
        {
            DB::table($table)
                ->insert($record);
        }

        return $this;
    }
}

