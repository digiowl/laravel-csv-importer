<?php

namespace DigiOwl\Importer;

use Illuminate\Support\ServiceProvider;

class ImportServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            Commands\ImportCsv::class
        ]);
    }
}
